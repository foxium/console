import { FooterConfig } from "./types";

const FooterBlock = (label: string, value: string, id: string) => `
    <div class="footer-block" id="${id}">
      <div class="footer-label">${label}</div>
      <div class="footer-value" id="bet-value">${value}</div>
    </div>
`;

export class Footer {
    constructor(readonly format: (number: number) => string, readonly config: FooterConfig) {
        document.querySelector<HTMLElement>(".footer").innerHTML = `
    <div style="display: flex; width: 100%; justify-content: space-between;">
    <div class="footer-block">
      <div class="footer-value" id="game-name-value"></div>
    </div>
    ${FooterBlock(this.config.texts.free_spins().toUpperCase(), "9/14", "footer-free-spins")}
</div>
<div style="display: flex; width: 100%; justify-content: space-between;">
    ${FooterBlock(this.config.texts.balance().toUpperCase(), this.format(0), "footer-balance")}
    ${FooterBlock(this.config.texts.bet().toUpperCase(), this.format(0), "footer-bet")}
</div>
`;
        this.setFreeSpinsVisible(false);
    }

    setGameName(value: string) {
        document.getElementById("game-name-value").innerText = value;
    }

    setBalance(value: number) {
        const block = document.getElementById("footer-balance");
        const betValue: HTMLElement = block.querySelector(".footer-value");
        betValue.innerText = this.format(value);
    }

    setBet(value: number) {
        const block = document.getElementById("footer-bet");
        const betValue: HTMLElement = block.querySelector(".footer-value");
        betValue.innerText = this.format(value);
    }

    setFreeSpinsVisible(visibility: boolean) {
        const block = document.getElementById("footer-free-spins");
        block.style.display = visibility ? "flex" : "none";
    }

    setFreeSpins(current: number, total: number) {
        const block = document.getElementById("footer-free-spins");
        const betValue: HTMLElement = block.querySelector(".footer-value");
        betValue.innerText = `${current} / ${total}`;
    }

    getFreeSpinsElementId() {
        return "footer-free-spins";
    }
}

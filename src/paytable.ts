import type { CTX } from "./types";
import { chunk, getPaytableFromPaylines } from "./utils";

type PaySymbol = {
    image: string;
    name: string;
    multiplier: [number, number, number];
};

export class Paytable {
    content = document.getElementById("paytable");
    bet = 0;

    constructor(readonly ctx: CTX) {}

    getPaySymbols(paySymbols: PaySymbol[]) {
        return `<div class="paytable-symbols-row">
          <div class="paytable-symbol-multipliers">
            <div class="paytable-symbol-multipliers-empty-space">

            </div>
            <div class="paytable-symbol-multipliers-value">
              ×5
            </div>
            <div class="paytable-symbol-multipliers-value">
              ×4
            </div>
            <div class="paytable-symbol-multipliers-value">
              ×3
            </div>
          </div>
          ${paySymbols
              .map((paySymbol) => {
                  return `<div class="paytable-symbol">
            <img class="paytable-symbol-image"
                 src="${paySymbol.image}"/>
            <h3 class="paytable-symbol-description-header">${paySymbol.name}</h3>
            <div class="paytable-symbol-description-block">
            ${paySymbol.multiplier
                .map((multiplier) => {
                    return `<div class="paytable-symbol-description-values-block">
                <div class="paytable-symbol-description-values-row">

                  <div class="paytable-symbol-description-values-row-amount">
                    ${this.ctx.format(multiplier * this.bet)}
                  </div>
                </div>
              </div>`;
                })
                .join("")}
            </div>
          </div>
`;
              })
              .join("")}
        </div>`;
    }

    getPaytableBlock(content: string, classNames?: string) {
        return `<div class="paytable-block ${classNames}">${content}</div>`;
    }

    getPaytableImage(url: string[]) {
        return `
<div class="paytable-image-line">
${url
    .map(
        (img) => `<img class="paytable-image"
                 src="${img}"/>`
    )
    .join("")}
</div>

`;
    }

    getHeader(text: string) {
        return `<h2 class="paytable-block-title">${text}</h2>`;
    }

    getDescription(text: string) {
        return `<div class="paytable-block-content">${text}</div>`;
    }

    testRender(bet: number) {
        const paylines: [number, number[]][] = [
            [0, [0, 1, 2, 3, 4]],
            [1, [5, 6, 7, 8, 9]],
            [2, [10, 11, 12, 13, 14]],
            [3, [15, 16, 17, 18, 19]],
            [4, [20, 21, 22, 23, 24]],
            [5, [0, 6, 2, 8, 4]],
            [6, [5, 11, 7, 13, 9]],
            [7, [10, 16, 12, 18, 24]],
            [8, [15, 21, 17, 23, 19]],
            [9, [5, 1, 7, 3, 9]],
            [10, [10, 6, 12, 8, 14]],
            [11, [15, 11, 17, 13, 19]],
            [12, [20, 16, 22, 18, 24]],
            [13, [0, 1, 7, 3, 4]],
            [14, [5, 6, 12, 8, 9]],
            [15, [10, 11, 17, 13, 14]],
            [16, [15, 16, 22, 18, 19]],
            [17, [5, 6, 2, 8, 9]],
            [18, [10, 11, 7, 13, 14]],
            [19, [15, 16, 12, 18, 19]],
            [20, [20, 21, 17, 23, 24]],
            [21, [0, 6, 7, 8, 4]],
            [22, [5, 11, 12, 13, 9]],
            [23, [10, 16, 17, 18, 14]],
            [24, [15, 21, 22, 23, 24]],
            [25, [5, 1, 2, 3, 9]],
            [26, [10, 6, 7, 8, 14]],
            [27, [15, 11, 12, 13, 19]],
            [28, [20, 16, 17, 18, 24]],
            [29, [0, 1, 7, 13, 14]],
            [30, [5, 6, 12, 18, 19]],
            [31, [10, 11, 17, 23, 24]],
            [32, [10, 11, 7, 3, 4]],
            [33, [15, 16, 12, 8, 9]],
            [34, [20, 21, 17, 13, 14]],
            [35, [0, 6, 7, 8, 14]],

            [36, [5, 11, 12, 13, 19]],
            [37, [10, 16, 17, 18, 24]],
            [38, [5, 11, 17, 13, 9]],
            [39, [15, 11, 7, 13, 19]],
        ];
        this.bet = bet;
        const paySymbols = [
            { image: "assets/High_1.png", name: "Stumpy McDoodles", multiplier: [4, 1.5, 0.8] },
            { image: "assets/High_2.png", name: "Penny McDoodles", multiplier: [2, 1, 0.5] },
            { image: "assets/High_3.png", name: "Shamrock", multiplier: [1.6, 0.8, 0.3] },
            { image: "assets/High_4.png", name: "Harp", multiplier: [1.2, 0.6, 0.2] },
            { image: "assets/Low_Ace.png", name: "Ace", multiplier: [0.6, 0.3, 0.12] },
            { image: "assets/Low_K.png", name: "King", multiplier: [0.6, 0.3, 0.12] },
            { image: "assets/Low_Queen.png", name: "Queen", multiplier: [0.4, 0.3, 0.1] },
            { image: "assets/Low_Jack.png", name: "Jack", multiplier: [0.4, 0.3, 0.1] },
        ];
        const isMobile = window.matchMedia("only screen and (max-width: 760px)").matches;
        const chunkedPaySymbols = chunk(paySymbols, isMobile ? 2 : 4);
        this.content.innerHTML = `

    ${this.getPaytableBlock(
        `
        ${this.getHeader("STUMPY RANDOM WILDS")}
        ${this.getPaytableImage(["assets/feature_random_wild_banner.png"])}
        ${this.getDescription(
            "This feature can trigger any time in base game only.<br/>3-12 Wilds may appear anywhere on the reels."
        )}
    `
    )}
            ${this.getPaytableBlock(
                `
            ${this.getHeader("Stumpy Treasure")}
            ${this.getPaytableImage(["assets/Chest._open.png"])}
            ${this.getDescription(`This feature can trigger in base game only. <br/>
When Chest symbol lands on reel 5, the prize of 1 – 100x bet is awarded.<br/>Wins awarded during the feature are added to base game wins.`)}
        `
            )}
            
                ${this.getPaytableBlock(
                    `
         ${this.getHeader("Cauldron Collection")}
         ${this.getPaytableImage(["assets/free_spins_collection_banner.png"])}
         ${this.getDescription(
             "Landing 2 Scatter symbols awards a Free Spins Golden Coin.<br/> Collect Golden Coins to increase number of Free Spins to play. <br/>Collected Golden Coins are stored per bet amount.<br/> " +
                 "No Golden Coins is awarded for 3 landed Scatter symbols."
         )}
    `
                )}
                
                
                
    ${this.getPaytableBlock(
        `
        ${this.getHeader("FREE SPINS")}
        ${this.getPaytableImage(["assets/feature_growing_wild_banner.png"])}
        ${this.getDescription(`Landing 3 Scatter symbols triggers Free Spins. <br/>
Number of Free Spins depends on Golden Coins collected.`)}
    `
    )}
            ${this.getPaytableBlock(
                `
        ${this.getHeader("Free Spins Growing Wild")}
        ${this.getPaytableImage(["assets/feature_random_wild_banner.png"])}
        ${this.getDescription(`
Free Spins starts with the 1 Wild on reels. <br/>
For every spin Wild may change position. <br/>
There is a chance to Collect Mug of Ale for any Free Spins. <br/>
If it happens the Wild will grow in size and 2 Extra spins are awarded.`)}
`
            )}
        
    ${this.getPaytableBlock(
        `
        ${this.getHeader("FREE SPINS SUPER WILD")}
        ${this.getPaytableImage(["assets/super_wild.png"])}
        ${this.getDescription(`When Wild grow to fill in the whole reel machine, it will become a Super Wild.<br/>
For the remaining Free Spins all 40 paylines 5 of a kind Wild payout will be awarded.`)}
`
    )}
            ${this.getPaytableBlock(`
            ${this.getHeader("Symbol payouts")}
        ${chunkedPaySymbols
            .map((chunk) => {
                return this.getPaySymbols(chunk as PaySymbol[]);
            })
            .join("")}
`)}

        ${this.getPaytableBlock(
            `
                ${this.getHeader("Wild Symbol")}
                ${this.getPaySymbols([
                    {
                        image: "assets/wild.png",
                        name: "Wild",
                        multiplier: [5, 3, 1],
                    },
                ])}
        ${this.getDescription(
            "Wilds can substitute for other symbols to complete winning combinations. <br/>Wilds can substitute for all symbols except Scatters and Chest symbols."
        )}
        `
        )}








    ${this.getPaytableBlock(
        `
            ${this.getHeader("40 paylines")}
            ${this
                .getDescription(`Line wins begin with the leftmost reel and pay left to right only on adjacent reels.<br/>
Only the highest win is paid per line.`)}
          ${getPaytableFromPaylines(paylines, { width: 5, height: 5 })}
          ${this.getDescription(
              "<br/>Some settings and features may not be available in this game. Malfunction voids all pays and plays."
          )}
    `,
        "paytable-paylines-block"
    )}


      `;
    }
}

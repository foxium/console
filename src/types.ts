export type PagesIds = "menu" | "autoplay" | "bets" | "settings" | "paytable";

export type Page = { id: PagesIds; title: string; icon: string };

export interface IBet {
    paylines: number;
    numChips: number;
    chipSize: number;
    value: number;
}

export type IBetValue = Partial<IBet> & { value: number };

export interface CTX {
    readonly format: (number: number) => string;
    openPage: (page: PagesIds) => void;
    onCloseClick: () => void;
    texts: Texts;
}

export interface IAutoplayPreset {
    spinCountPresets: number[];
    lossLimitPresets: string[];
    winLimitPresets: string[];
}

export type AutoplayState = {
    spins?: number;
    lossLimit?: string;
    winLimit?: string;
};

export type NotificationProps = {
    message: string;
    cb?: () => void;
};

export interface FooterConfig {
    texts: Texts;
}

export interface ModalSystemConfig {
    onCloseCb?: () => void;
    // onSoundToggle?: (isActive: boolean) => void;
    texts: Texts;
    autoplay?: {
        isEnabled: boolean;
        presets?: IAutoplayPreset;
        onStart?: (autoplayState: AutoplayState) => void;
    };
    audioPlayer?: {
        isEnabled: boolean;
        toggle?: (isEnabled: boolean) => void;
    };
    splashSettings?: {
        isEnabled: boolean;
        toggle: (isEnabled: boolean) => boolean;
    };
    banking?: {
        isEnabled: boolean;
        launch: () => void;
    };
    lobby?: {
        isEnabled: boolean;
        launch: () => void;
    };
}

export type Texts = Partial<{
    cancel: () => string;
    game_title_multiline: () => string;
    start_autoplay: () => string;
    select: () => string;
    add_13_1: (data: [string, string]) => string;
    ll_bet_over_limit: () => string;
    none: () => string;
    fshelp_title2: () => string;
    balance: () => string;
    price: () => string;
    menu_audio: () => string;
    quickspin_dialog_title: () => string;
    fortune_text: () => string;
    noWebGLMessage: () => string;
    autoplay_limit: () => string;
    game_title: () => string;
    bets: () => string;
    content_paytable_title: () => string;
    triton_text: () => string;
    click: () => string;
    fortune_title: () => string;
    win_free_spins: (data: [string]) => string;
    buy_price: (data: { num: string }) => string;
    switching_to_free_spins: (data: [string, string]) => string;
    lines_text: () => string;
    tooltip_boost: () => string;
    symbol_high1: () => string;
    symbol_high2: () => string;
    wincalc_text: () => string;
    noWebGLTitle: () => string;
    symbol_high3: () => string;
    symbol_high4: () => string;
    menu_home: () => string;
    add_11: (data: { num: string }) => string;
    add_12: () => string;
    content_settings_title: () => string;
    add_10: () => string;
    feature_up_to: () => string;
    singlewin_limit: () => string;
    symbol_wild: () => string;
    fshelp_text: () => string;
    add_08: (data: { num: string }) => string;
    add_09: (data: { num: string }) => string;
    add_06: () => string;
    add_07: () => string;
    bonus_game_banner_message: () => string;
    fast_play: () => string;
    win_val: (data: { num: string }) => string;
    add_04: (data: { num: string }) => string;
    add_05: () => string;
    add_02: (data: { num: string }) => string;
    add_03: (data: { num: string }) => string;
    badge_text_1: () => string;
    audio: () => string;
    badge_text_4: () => string;
    badge_text_5: () => string;
    badge_text_2: () => string;
    badge_text_3: () => string;
    upto: () => string;
    select_spins: () => string;
    error_limits: () => string;
    until_stop: () => string;
    win_upcase: () => string;
    no_limit: () => string;
    scatter_symbol: () => string;
    wild_title: () => string;
    win_big: () => string;
    disclaimer_feature: () => string;
    fshelp_text2: () => string;
    tooltip_quick_active: () => string;
    spin: () => string;
    reached_swl: () => string;
    bonus_game_banner_free_spin_message: () => string;
    disclaimer_title: () => string;
    bonus_game_banner_discount_message: () => string;
    buy: () => string;
    error_session: () => string;
    add_31: () => string;
    settings_title: () => string;
    add_30: () => string;
    bet: () => string;
    error_network: () => string;
    buy_disclaimer: (data: { amount: string; bet: string }) => string;
    add_28: () => string;
    add_29: () => string;
    add_26: () => string;
    add_27: () => string;
    add_24: () => string;
    continue: () => string;
    tooltip_boost_disable: () => string;
    add_25: () => string;
    continue_upper: () => string;
    tooltip_boost_active: () => string;
    symbol_triton: () => string;
    menu_settings: () => string;
    settings: () => string;
    fshelp_title: () => string;
    win_up_to: (data: { num: string }) => string;
    bonus_balance: () => string;
    play4real: () => string;
    add_22: () => string;
    add_23: (data: { num: string }) => string;
    add_20: () => string;
    add_21: (data: { num: string }) => string;
    menu_autoplay: () => string;
    cash_balance: () => string;
    autoplay: () => string;
    add_19: () => string;
    win_epic: () => string;
    add_17: () => string;
    content_auto_title: () => string;
    settings_info: () => string;
    add_18: () => string;
    add_15: () => string;
    add_16: () => string;
    add_13: (data: [string, string, string]) => string;
    add_14: () => string;
    respin: () => string;
    error_locked: () => string;
    no: () => string;
    memory: () => string;
    enable_audio: () => string;
    skip_title: () => string;
    tooltip_buyout: () => string;
    congratulations: () => string;
    reached_ll: () => string;
    win_mega: () => string;
    back: () => string;
    symbols: () => string;
    banking: () => string;
    bet_reset: () => string;
    free_spins: () => string;
    quickspin_dialog_text: () => string;
    cashier: () => string;
    menu_help: () => string;
    win_discount: (data: [string]) => string;
    lines: () => string;
    ok: () => string;
    wincalc_title: () => string;
    game_title_upcase: () => string;
    win: () => string;
    orientation: () => string;
    menu_paytable: () => string;
    tooltip_setting: () => string;
    wild_text: () => string;
    loss_limit: () => string;
    yes: () => string;
    start: () => string;
    win_total: () => string;
    paytable: () => string;
    lines_title: () => string;
    symbol_low2: () => string;
    have_won: () => string;
    symbol_low1: () => string;
    symbol_low4: () => string;
    symbol_low3: () => string;
    content_bets_title: () => string;
    better_luck: () => string;
    symbol_low5: () => string;
}>;

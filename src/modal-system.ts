import type { CTX, ModalSystemConfig, Page, PagesIds, Texts } from "./types";
import { Bets } from "./bets";
import { Settings } from "./settings";
import { Menu } from "./menu";
import { Paytable } from "./paytable";
import { Autoplay } from "./autoplay";
import { Notify } from "./notify";

export class ModalSystem implements CTX {
    // Grand parent
    consoleOverlay = document.getElementById("console-overlay");

    // Parent of all except FOOTER!
    closeButton = document.getElementById("header-close-button");
    overlayModal = document.getElementById("overlay-modal");
    title = document.getElementById("header-title");

    bets: Bets;
    settings: Settings;
    menu: Menu;
    paytable: Paytable;
    autoplay: Autoplay;
    notify: Notify;

    audioFromSettings: { applyState: (isChecked: boolean) => void };
    audioFromMenu: { applyState: (isChecked: boolean) => void };

    pages = new Map<PagesIds, Page>();
    texts: Texts;

    constructor(readonly format: (number: number) => string, readonly config: ModalSystemConfig) {
        this.texts = config.texts;
        this.bets = new Bets(this);
        this.settings = new Settings(this);
        this.menu = new Menu(this);
        this.paytable = new Paytable(this);
        if (this.config.autoplay?.isEnabled) {
            this.autoplay = new Autoplay(this, this.config.autoplay.presets, this.config.autoplay.onStart);
        }
        this.notify = new Notify(this);
        this.setupPages();
        this.setupMenu();
        this.setupAudioControls();
        this.setupSplashControls();

        this.closeButton.addEventListener("click", this.onCloseClick.bind(this));
    }

    setupPages() {
        this.pages.set("menu", {
            title: "Menu",
            id: "menu",
            icon: "./assets/menu-ico.svg",
        });
        this.pages.set("autoplay", {
            title: this.texts.autoplay(),
            id: "autoplay",
            icon: "assets/autoplay-icon.svg",
        });
        this.pages.set("bets", {
            title: this.texts.bets(),
            id: "bets",
            icon: "./assets/bet-icon-on.svg",
        });
        this.pages.set("settings", {
            title: this.texts.settings(),
            id: "settings",
            icon: "./assets/setting-icon.svg",
        });
        this.pages.set("paytable", {
            title: this.texts.paytable(),
            id: "paytable",
            icon: "./assets/paytable-icon-on.svg",
        });
    }

    setupMenu() {
        const { texts, banking, lobby } = this.config;

        if (lobby?.isEnabled) {
            this.menu.createRow(texts.menu_home(), "./assets/lobby-icon.svg", lobby.launch);
        }

        this.menu.addLink(this.pages.get("settings"));
        this.menu.addLink(this.pages.get("paytable"));

        if (banking?.isEnabled) {
            this.menu.createRow(texts.banking(), "./assets/banking-icon.svg", banking.launch);
        }
    }

    openPage(pageId: PagesIds) {
        const page = this.pages.get(pageId);
        const pageNode = document.getElementById(page.id);

        this.overlayModal.classList.remove("close");
        this.enableClicks();
        this.closeAllPages();
        pageNode.classList.remove("display-none");
        this.updateTitle(page.title, page.icon);
    }

    onCloseClick() {
        this.config.onCloseCb?.();
        this.disableClicks();
        this.closeAllPages();
        this.overlayModal.classList.add("close");
    }

    enableClicks() {
        this.consoleOverlay.classList.remove("pointer-events-none");
    }

    disableClicks() {
        this.consoleOverlay.classList.add("pointer-events-none");
    }

    updateTitle(title: string, icon: string) {
        // TODO: move it to css file;
        this.title.innerHTML = `<img style="width: 1em; height: auto;padding: 0; margin: 0;  padding-right: 0.5em;" src="${icon}" /> ${title}`;
    }

    closeAllPages() {
        this.pages.forEach((page) => {
            const pageNode = document.getElementById(page.id);
            pageNode.classList.add("display-none");
        });
    }

    private setupAudioControls() {
        const title = this.texts.audio();
        this.audioFromSettings = this.settings.addSetting({
            title,
            onToggle: this.onAudioToggle,
            defaultState: this.config.audioPlayer?.isEnabled ?? false,
        });
        this.audioFromMenu = this.menu.addCb({
            title,
            icons: { enabled: "./assets/sound-on.svg", disabled: "./assets/sound-off.svg" },
            onToggle: this.onAudioToggle,
            defaultState: this.config.audioPlayer?.isEnabled ?? false,
        });
    }
    public onAudioToggle = (isEnabled: boolean) => {
        this.audioFromSettings?.applyState(isEnabled);
        this.audioFromMenu?.applyState(isEnabled);
        this.config.audioPlayer?.toggle?.(isEnabled);
    };

    private setupSplashControls() {
        if (this.config.splashSettings) {
            this.settings.addSetting({
                title: this.texts.settings_title(),
                onToggle: this.config.splashSettings.toggle,
                defaultState: this.config.splashSettings.isEnabled,
            });
        }
    }
}

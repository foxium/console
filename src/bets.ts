import type { CTX, IBet, IBetValue } from "./types";
import { Button } from "./controls/templates";

const reduceBetsArrayLength = (listOfBets: IBetValue[], expectedLength: number) => {
    const bets = Array.from(listOfBets.values()).sort((prevBet: IBet, nextBet: IBet) => prevBet.value - nextBet.value);
    const maxNumberOfBets = expectedLength;
    if (bets.length >= maxNumberOfBets) {
        const arrMinMax: IBetValue[] = [bets[0], bets[bets.length - 1]];
        let reducePicker = 1;
        while (bets.length + arrMinMax.length > maxNumberOfBets) {
            bets.splice(bets.length - ++reducePicker, 1);
            if (reducePicker >= bets.length) reducePicker = 0;
        }

        bets.push(arrMinMax[1]);
        bets.unshift(arrMinMax[0]);
    }

    return bets;
};

class QuickBetsList {
    list = document.getElementById("bets-list");
    listOfQuickBets: IBetValue[] = [];

    constructor(readonly onClick: (value: number) => void, readonly ctx: CTX) {}

    setup(listOfQuickBets: IBetValue[]) {
        this.listOfQuickBets = listOfQuickBets;
    }

    pickBet(bet: IBetValue) {
        this.list.innerHTML = this.listOfQuickBets
            .map((option) => Button(this.ctx.format(option.value), `bet-${option.value}`))
            .join("");
        for (let i = 0; i < this.list.children.length; i++) {
            const betNode = this.list.children[i];
            const betValue = +betNode.id.split("bet-")[1];
            if (betValue === bet.value) {
                betNode.classList.add("bet-active");
            }

            betNode.addEventListener("click", () => {
                for (let j = 0; j < this.list.children.length; j++) {
                    this.list.children[j].classList.remove("bet-active");
                }
                betNode.classList.add("bet-active");
                this.onClick(betValue);
            });
        }
    }
}

export class Bets {
    content = document.getElementById("bets");
    label = document.getElementById("bet-counter-label");
    incrementBtn = document.getElementById("bet-increment");
    decrementBtn = document.getElementById("bet-decrement");
    minBtn = document.getElementById("bet-set-min");
    maxBtn = document.getElementById("bet-set-max");

    listOfBets: IBetValue[];
    quickBets: QuickBetsList;

    constructor(readonly ctx: CTX) {
        this.quickBets = new QuickBetsList((value) => {
            this.currentBet = this.listOfBets.find((el) => el.value === value);
            setTimeout(() => this.ctx.onCloseClick(), 500);
        }, this.ctx);
        this.incrementBtn.addEventListener("click", this.increment.bind(this));
        this.decrementBtn.addEventListener("click", this.decrement.bind(this));
        this.minBtn.addEventListener("click", this.setMin.bind(this));
        this.maxBtn.addEventListener("click", this.setMax.bind(this));
    }

    _currentBet: IBetValue;

    get currentBet(): IBetValue {
        return this._currentBet;
    }

    set currentBet(value: IBetValue) {
        this._currentBet = value;
        this.quickBets.pickBet(value);
        this.updateCounter();
        this.callback(value);
    }

    callback = (value: IBetValue) => console.log("add callback " + value);

    updateCounterLabel() {
        this.label.innerHTML = `<div class="bet-value">${this.ctx.format(this.currentBet.value)}</div>`;
    }

    updateCounter() {
        this.updateCounterLabel();
        const index = this.listOfBets.findIndex((el) => el.value === this.currentBet.value);
        this.incrementBtn.classList.remove("disable");
        this.decrementBtn.classList.remove("disable");
        this.maxBtn.classList.remove("bet-active");
        this.minBtn.classList.remove("bet-active");
        if (index === 0) {
            this.decrementBtn.classList.add("disable");
            this.minBtn.classList.add("bet-active");
        }
        if (index === this.listOfBets.length - 1) {
            this.incrementBtn.classList.add("disable");
            this.maxBtn.classList.add("bet-active");
        }
        this.callback(this.currentBet);
    }

    setMin() {
        this.currentBet = this.listOfBets[0];
    }

    setMax() {
        this.currentBet = this.listOfBets[this.listOfBets.length - 1];
    }

    increment() {
        const index = this.listOfBets.findIndex((el) => el.value === this.currentBet.value);
        this.currentBet = this.listOfBets[index + 1];
    }

    decrement() {
        const index = this.listOfBets.findIndex((el) => el.value === this.currentBet.value);
        this.currentBet = this.listOfBets[index - 1];
    }

    setup(options: IBetValue[], currentBet: IBetValue, cb?: (value: IBetValue) => void) {
        this.listOfBets = options;

        this.quickBets.setup(reduceBetsArrayLength(options, 10));
        this.currentBet = currentBet;

        this.updateCounter();
        this.callback = cb;
    }
}

import type { CTX } from "./types";
import { NotificationProps } from "./types";

export class Notify {
    content = document.getElementById("foxium-notify");
    isActive = false;

    constructor(readonly ctx: CTX) {}

    // TODO some weird things, display & animations logic should be rewritten
    notify({ message, cb }: NotificationProps) {
        if (this.isActive) return;
        this.isActive = true;
        this.content.style.display = "flex";
        const msgText = this.content.querySelector("#foxium-notify-message");
        msgText.innerHTML = message;

        const msgBg = this.content.querySelector("#foxium-notify-message-bg") as HTMLElement;
        msgBg.style.opacity = "1";

        let timeout: any = null;

        const onNotifyClose = () => {
            document.removeEventListener("click", onNotifyClose);
            clearTimeout(timeout);
            msgBg.style.opacity = "0";

            setTimeout(() => {
                cb?.();
                this.content.style.display = "none";
                this.isActive = false;
            }, 700);
        };

        document.addEventListener("click", onNotifyClose);
        timeout = setTimeout(onNotifyClose, 2.5 * 1000);
    }
}

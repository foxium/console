import type { CTX } from "./types";

export class Settings {
    content = document.getElementById("settings");
    list = document.getElementById("settings-list");

    constructor(readonly ctx: CTX) {}

    addSetting({
        title,
        onToggle,
        defaultState,
    }: {
        title: string;
        onToggle: (state: boolean) => void;
        defaultState: boolean;
    }) {
        const idCheckbox = Math.random().toString(36).substr(2);
        const settingsRow = document.createElement("div");
        settingsRow.classList.add("settings-row");
        settingsRow.innerHTML = `
            <div class="settings-title">${title}</div>
            <label class="switch">
               <input type="checkbox" id="${idCheckbox}">
                  <span class="switch-slide"></span>
            </label>
    `;

        this.list?.appendChild(settingsRow);
        const getCheckbox = () => document.getElementById(idCheckbox) as HTMLInputElement;
        getCheckbox().addEventListener("click", () => onToggle(getCheckbox().checked));

        const applyState = (isChecked: boolean) => {
            getCheckbox().checked = isChecked;
        };
        applyState(defaultState);

        return { applyState };
    }
}

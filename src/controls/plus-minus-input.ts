import { kebabize, randToken } from "../utils";

export class PlusMinusInput {
    options: string[];
    isInit = false;
    private minusId = randToken();
    private plusId = randToken();
    private readoutId = randToken();

    constructor(options: string[], defaultIndex?: number, readonly onChange?: (value: string, index: number) => void) {
        this.setup(options, defaultIndex);
    }

    _valueIndex: number;

    get valueIndex() {
        return this._valueIndex;
    }

    set valueIndex(index: number) {
        this._valueIndex = index;
    }

    setup(options: string[], defaultIndex?: number) {
        this.options = options;
        if (defaultIndex) {
            this.valueIndex = defaultIndex;
        } else {
            this.valueIndex = 0;
        }
    }

    _onChange() {
        this.render();
        this.onChange?.(this.options[this.valueIndex], this.valueIndex);
        return this.options[this.valueIndex];
    }

    increment() {
        this.valueIndex += 1;
        return this._onChange();
    }

    decrement() {
        this.valueIndex -= 1;
        return this._onChange();
    }

    init() {
        if (!this.isInit) {
            this.isInit = true;
            const decrementBtn = document.getElementById(this.minusId);
            decrementBtn.addEventListener("click", this.decrement.bind(this));
            const incrementBtn = document.getElementById(this.plusId);
            incrementBtn.addEventListener("click", this.increment.bind(this));

            this._onChange();
        }
    }

    render() {
        this.init();

        const decrementBtn = document.getElementById(this.minusId);
        const incrementBtn = document.getElementById(this.plusId);
        const readoutLabel = document.getElementById(this.readoutId);
        if (decrementBtn && incrementBtn && readoutLabel) {
            if (this.valueIndex === 0) {
                decrementBtn.classList.add("disable");
            } else {
                decrementBtn.classList.remove("disable");
            }

            if (this.valueIndex === this.options.length - 1) {
                incrementBtn.classList.add("disable");
            } else {
                incrementBtn.classList.remove("disable");
            }
            readoutLabel.innerText = this.options[this.valueIndex];
        }
    }

    html(styles?: Partial<CSSStyleDeclaration>) {
        // Hack for innerHtml = html();
        setTimeout(this.render.bind(this), 1);
        return `
            <div class="plus-minus-input-block" style="${styleObjToString(styles)}">
                <div class="plus-minus-input-control" id="${this.minusId}">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" style="width: 1em; height: 1em;margin: auto auto; fill: white;" viewBox="0 0 24 24"><path d="M0 10h24v4h-24z" fill="white"/></svg>
                </div>
                <div class="bet-button plus-minus-input-readout">
                <div class="bet-value" id="${this.readoutId}">
                </div>
                </div>
                <div class="plus-minus-input-control" id="${this.plusId}" >
                    <svg xmlns="http://www.w3.org/2000/svg" style="margin: auto auto;fill: white;width: 1em; height: 1em;" width="24" height="24" viewBox="0 0 24 24"><path d="M24 10h-10v-10h-4v10h-10v4h10v10h4v-10h10z"/></svg>
                </div>
            </div>
`;
    }
}

const styleObjToString = (styles: Partial<CSSStyleDeclaration>) => {
    const stylesObj = styles ?? {};
    return Object.keys(stylesObj).map(() =>
        Object.keys(stylesObj)
            .map((key) => `${kebabize(key)}: ${styles[key]};`)
            .join("")
    );
};

import { kebabize } from "../utils";

export const Button = (value: string, id: string, styles?: Partial<CSSStyleDeclaration>) =>
    `<div class="bet-button" style="${Object.keys(styles ?? {})
        .map((key) => `${kebabize(key)}: ${styles[key]};`)
        .join("")}" id=${id}><div class="bet-value">${value}</div></div>`;

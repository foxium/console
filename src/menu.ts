import { CTX, Page } from "./types";

export class Menu {
    content = document.getElementById("menu");
    list = document.getElementById("menu-list");

    constructor(readonly ctx: CTX) {
        // Menu icon
    }

    createRow(title: string, icon: string, onClick: () => void) {
        const menuRow = document.createElement("div");
        menuRow.classList.add("menu-row");
        menuRow.innerHTML = `
          <img class="menu-item-icon" src="${icon}" />
          <div class="menu-buttons-title">${title}</div>
    `;
        menuRow.addEventListener("click", onClick ?? function () {});
        this.list.appendChild(menuRow);
        return menuRow;
    }

    addCb({
        title,
        icons,
        onToggle,
        defaultState,
    }: {
        title: string;
        icons: { enabled: string; disabled: string };
        onToggle: (state: boolean) => void;
        defaultState: boolean;
    }) {
        // closure
        let state = defaultState ?? true;
        const getIconByState = (state: boolean) => (state ? icons.enabled : icons.disabled);
        const applyState = (newState: boolean) => {
            state = newState;
            changeIcon(state);
        };
        const menuRow = this.createRow(title, getIconByState(state), () => {
            applyState(!state);
            onToggle(state);
        });

        const changeIcon = (state: boolean) =>
            ((menuRow.getElementsByClassName("menu-item-icon")[0] as HTMLImageElement).src = getIconByState(state));

        applyState(state);

        return { applyState };
    }

    addLink(page: Page) {
        this.createRow(page.title, page.icon, () => this.ctx.openPage(page.id));
    }
}

export const chunk = <T>(input: T[], size: number) => {
    return input.reduce((arr: T[][], item, idx) => {
        return idx % size === 0 ? [...arr, [item]] : [...arr.slice(0, -1), [...arr.slice(-1)[0], item]];
    }, []);
};

const getPaytableSlot = ({ width, height }: { width: number; height: number }, payline: number[], index: number) => {
    const array = Array.from({ length: width * height }, (_, i) => i);
    const workArray = chunk(array, width);
    return `<div>
<div style="font-size: 1.2em">${index + 1}</div>
<svg width="${26.6 * width}" height="${26.6 * height}" viewBox="0 0 ${26.6 * width} ${
        26.6 * height
    }" fill="none" xmlns="http://www.w3.org/2000/svg">
${workArray.map((row, rowI) => {
    return row.map((element: number, elementI) => {
        const paylineDot = payline.includes(element);
        const fill = paylineDot ? 'fill="white" ' : 'fill="none"';

        return `<rect x="${1 + elementI * 27}" y="${
            1 + rowI * 27
        }" width="23" height="23" ${fill} stroke="#7e8491" stroke-width="2"/>`;
    });
})}
</svg>
</div>
`;
};

type Paylines = [number, number[]][];

export const getPaytableFromPaylines = (paylines: Paylines, size: { width: number; height: number }) =>
    paylines
        .map((el) => el[1])
        .map((el: number[], index) => getPaytableSlot(size, el, index))
        .join("");

export const randToken = () => Math.random().toString(36).substr(2);

export const kebabize = (str: string) =>
    str.replace(/[A-Z]+(?![a-z])|[A-Z]/g, ($, ofs) => (ofs ? "-" : "") + $.toLowerCase());

import { PlusMinusInput } from "./controls/plus-minus-input";
import { AutoplayState, CTX, IAutoplayPreset } from "./types";
import { Button } from "./controls/templates";

export class Autoplay {
    content = document.getElementById("autoplay");

    state: AutoplayState = {};

    constructor(
        readonly ctx: CTX,
        readonly autoplayPresets: IAutoplayPreset,
        readonly callback?: (autoplayState: AutoplayState) => void
    ) {
        console.log(autoplayPresets);
        this.render();
    }

    appendState(state: AutoplayState) {
        Object.assign(this.state, state);
        this._onStateChange();
    }

    _onStateChange() {
        const { spins, lossLimit, winLimit } = this.state;
        const sumbitBtn = document.getElementById("autoplay-sumbit-button");
        if (spins && lossLimit && winLimit) {
            sumbitBtn.classList.remove("disabled");
        } else {
            sumbitBtn.classList.add("disabled");
        }
    }

    preCreateElements() {
        const lossLimitPlusMinus =
            this.autoplayPresets.lossLimitPresets?.length &&
            new PlusMinusInput(
                this.autoplayPresets.lossLimitPresets.map((el) => (isFinite(+el) ? this.ctx.format(+el) : el)),
                0,
                (value, index) => {
                    this.appendState({ lossLimit: this.autoplayPresets.lossLimitPresets[index] });
                }
            );

        const winLimitPlusMinus =
            this.autoplayPresets.winLimitPresets?.length &&
            new PlusMinusInput(
                this.autoplayPresets.winLimitPresets.map((el) => (isFinite(+el) ? this.ctx.format(+el) : el)),
                0,
                (value, index) => {
                    this.appendState({ winLimit: this.autoplayPresets.winLimitPresets[index] });
                }
            );

        return { lossLimitPlusMinus, winLimitPlusMinus };
    }

    render() {
        const { winLimitPlusMinus, lossLimitPlusMinus } = this.preCreateElements();

        const buttonsPerRow = this.autoplayPresets.spinCountPresets?.length ?? 4;

        this.content.innerHTML = `
      <div style="padding: 20px 0;margin: auto auto; width: 100%;height: 100%; max-height: 400px; justify-content: space-between; display: flex; flex-direction: column" id="autoplay-content">
      
        <div class="autoplay-spins-block">
          <h3 class="elements-label" style="text-align: left;">${this.ctx.texts.select_spins()}</h3>
          <div class="bets-block" style="grid-template-columns: repeat(${buttonsPerRow}, 1fr)">
            ${Array.from(this.autoplayPresets.spinCountPresets?.values() ?? [], (el, i) =>
                Button(`${el}`, `auto-spins-${el}`)
            ).join("")}
          </div>
        </div>
        <div class="autoplay-limits-block" >
          ${
              lossLimitPlusMinus?.html
                  ? `
            <div style="width: 100%;">
              <h3 class="elements-label">${this.ctx.texts.loss_limit()}</h2>
              <div>${lossLimitPlusMinus.html()}</div>
            </div>
          `
                  : ""
          }
          
          ${
              winLimitPlusMinus?.html
                  ? `
            <div style="width: 100%;">
              <h3 class="elements-label">${this.ctx.texts.singlewin_limit()}</h3>
              <div>${winLimitPlusMinus.html()}</div>
            </div>
          `
                  : ""
          }
        </div>
        
        <div class="bet-button"  id="autoplay-sumbit-button">
            <div class="bet-value">${this.ctx.texts.start().toUpperCase()}</div>
        </div>
      </div>
    `;

        this.autoplayPresets.spinCountPresets?.forEach((spins) => {
            const btn = document.getElementById(`auto-spins-${spins}`);
            btn.addEventListener("click", () => {
                this.autoplayPresets.spinCountPresets?.forEach((spins) => {
                    document.getElementById(`auto-spins-${spins}`).classList.remove("bet-active");
                });
                btn.classList.add("bet-active");
                this.appendState({ spins });
            });
        });

        document.getElementById("autoplay-sumbit-button").addEventListener("click", () => {
            this.callback?.(this.state);
            this.ctx.onCloseClick();
            this.state = {};
            this.render();
        });
    }
}

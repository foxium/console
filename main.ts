import { Footer, ModalSystem } from "./src";

const footer = new Footer((value) => `$${(value / 100).toFixed(2)}`, {
    texts: { balance: () => "Balance", bet: () => "Bet", free_spins: () => "Free spins" },
});
footer.setGameName("Stumpy");
const modalSystem = new ModalSystem((value) => `$${(value / 100).toFixed(2)}`, {
    texts: {
        autoplay: () => "Autoplay",
        audio: () => "Audio",
        bets: () => "Bets",
        paytable: () => "Paytable",
        settings: () => "Settings",
        select_spins: () => "Spins",
        loss_limit: () => "Loss limit",
        singlewin_limit: () => "Win limit",
        start: () => "Start",
        settings_title: () => "Show splash screen",
        banking: () => "Banking",
        menu_home: () => "Home",
    },
    autoplay: {
        isEnabled: true,
        onStart: (state) => console.log(state),
        presets: {
            winLimitPresets: ["None", "10000", "25000", "50000", "100000", "999900"],
            lossLimitPresets: ["10000", "25000", "50000", "100000", "999900"],
            spinCountPresets: [10, 25, 50, 100],
        },
    },
    audioPlayer: {
        isEnabled: false,
        toggle: (active: boolean) => console.log(active, "works amazing"),
    },

    splashSettings: {
        isEnabled: false,
        toggle: (isEnabled: boolean) => {
            console.log("SplashSettings toggle", isEnabled);
            return true;
        },
    },
    banking: {
        isEnabled: true,
        launch: () => {
            console.log("Banking launch");
        },
    },
    lobby: {
        isEnabled: true,
        launch: () => {
            console.log("Lobby launch");
        },
    },
});
modalSystem.openPage("paytable");
modalSystem.bets.setup(
    Array.from({ length: 50 }, (_, i) => ({ value: (i + 1) * 100 })),
    { value: 100 },
    (value) => console.log(value)
);
modalSystem.bets.setup(
    [
        { value: 20, chipSize: 1, numChips: 1, paylines: 20 },
        { value: 40, chipSize: 2, numChips: 1, paylines: 20 },
        { value: 60, chipSize: 1, numChips: 3, paylines: 20 },
        { value: 80, chipSize: 2, numChips: 2, paylines: 20 },
        { value: 100, chipSize: 5, numChips: 1, paylines: 20 },
        { value: 200, chipSize: 10, numChips: 1, paylines: 20 },
        { value: 300, chipSize: 5, numChips: 3, paylines: 20 },
        { value: 400, chipSize: 20, numChips: 1, paylines: 20 },
        { value: 500, chipSize: 25, numChips: 1, paylines: 20 },
        { value: 600, chipSize: 10, numChips: 3, paylines: 20 },
        { value: 800, chipSize: 20, numChips: 2, paylines: 20 },
        { value: 1000, chipSize: 50, numChips: 1, paylines: 20 },
        { value: 1500, chipSize: 25, numChips: 3, paylines: 20 },
        { value: 2000, chipSize: 100, numChips: 1, paylines: 20 },
        { value: 2500, chipSize: 25, numChips: 5, paylines: 20 },
        { value: 3000, chipSize: 50, numChips: 3, paylines: 20 },
        { value: 4000, chipSize: 100, numChips: 2, paylines: 20 },
        { value: 5000, chipSize: 50, numChips: 5, paylines: 20 },
        { value: 6000, chipSize: 100, numChips: 3, paylines: 20 },
        { value: 8000, chipSize: 100, numChips: 4, paylines: 20 },
        { value: 10000, chipSize: 100, numChips: 5, paylines: 20 },
        { value: 12000, chipSize: 100, numChips: 6, paylines: 20 },
    ],
    { value: 100 }
);

modalSystem.paytable.testRender(300);
window.addEventListener("keydown", (event) => {
    if (event.key === "n") {
        modalSystem.notify.notify({ message: "Win limit reached", cb: () => {} });
    }
});
modalSystem.openPage("paytable");
